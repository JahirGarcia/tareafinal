﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ValueObjects
{
    public enum MetodoDepreciacion
    {
        LINEA_RECTA,
        SUMA_DE_DIGITOS_ANUALES,
        DOBLE_SALDO_DECRECIENTE
    }
}
