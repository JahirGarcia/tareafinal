﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ValueObjects
{
    public enum TipoActivoFijo
    {
        EDIFICIO_INDUSTRIAL = 10,
        EDIFICIO_COMERCIAL = 20,
        RESIDENCIA_EN_EXPLOTACIONES_AGROPECUARIAS = 10,
        INSTALACION_EN_EXPLOTACIONES_AGROPECUARIAS = 10,
        EDIFICIO_DE_ALQUILER = 30,
        TRANSPORTE_COLECTIVO_O_DE_CARGA = 5,
        VEHICULO_DE_EMPRESA_DE_ALQUILER = 3,
        VEHICULO_DE_USO_PARTICULAR = 5,
        OTRO_EQUIPO_DE_TRANSPORTE = 8,
        BIEN_INMOVIL = 10,
        MAQUINARIA_Y_EQUIPO_NO_ADHERIDO_PERMANENTEMENTE = 7,
        EQUIPO_DE_EMPRESA_AGROINDUSTRIAL = 5,
        MAQUINARIA_AGRICOLA = 5,
        MOBILIARIO_Y_EQUIPO_DE_OFICINA = 5,
        EQUIPO_DE_COMUNICACION = 5,
        ASCENSOR_ELEVADOR_Y_UNIDAD_DE_AIRE_ACONDICIONADO = 10,
        EQUIPO_DE_COMPUTO = 2,
        EQUIPO_PARA_MEDIOS_DE_COMUNICACION = 2,
        OTRA_MAQUINARIA_Y_EQUIPO = 5,
    }
}
