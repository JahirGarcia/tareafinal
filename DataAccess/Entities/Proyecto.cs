﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.ValueObjects;

namespace DataAccess.Entities
{
    public class Proyecto
    {
        private int id;//4
        private string nombre;//32 => 67
        private string descripcion;//100 => 203
        private double invercionInicial;//8
        private MetodoDepreciacion metodoDepreciacion;//32 => 67
        private double tmar;//8
        private int periodo;//4
        //Total => 361

        public Proyecto(int id, string nombre, string descripcion, double invercionInicial, MetodoDepreciacion metodoDepreciacion, double tmar, int periodo)
        {
            this.id = id;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.invercionInicial = invercionInicial;
            this.metodoDepreciacion = metodoDepreciacion;
            this.tmar = tmar;
            this.periodo = periodo;
        }

        public Proyecto() { }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double InvercionInicial { get => invercionInicial; set => invercionInicial = value; }
        public MetodoDepreciacion MetodoDepreciacion { get => metodoDepreciacion; set => metodoDepreciacion = value; }
        public double Tmar { get => tmar; set => tmar = value; }
        public int Periodo { get => periodo; set => periodo = value; }
        public int Id { get => id; set => id = value; }
        //Total =>
    }
}
