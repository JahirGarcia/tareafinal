﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Ingreso
    {
        private int id;//4
        private int año;//4
        private double monto;//8
        private Proyecto proyecto;//4
        //Total => 20

        public Ingreso(int id, int año, double monto, Proyecto proyecto)
        {
            this.id = id;
            this.año = año;
            this.monto = monto;
            this.proyecto = proyecto;
        }

        public Ingreso() { }

        public int Id { get => id; set => id = value; }
        public int Año { get => año; set => año = value; }
        public double Monto { get => monto; set => monto = value; }
        public Proyecto Proyecto { get => proyecto; set => proyecto = value; }
    }
}
