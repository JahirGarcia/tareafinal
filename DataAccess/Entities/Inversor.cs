﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Inversor
    {
        private int id;//4
        private string nombre;//32 => 67
        private double aportacion;//8
        private double aportacionPorcentual;//8
        private double rendimiento;//8
        private double promedioPonderado;//8
        private Proyecto proyecto;//4
        //Total => 107

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public double Aportacion { get => aportacion; set => aportacion = value; }
        public double AportacionPorcentual { get => aportacionPorcentual; set => aportacionPorcentual = value; }
        public double Rendimiento { get => rendimiento; set => rendimiento = value; }
        public double PromedioPonderado { get => promedioPonderado; set => promedioPonderado = value; }
        public Proyecto Proyecto { get => proyecto; set => proyecto = value; }

        public Inversor(int id, string nombre, double aportacion, double aportacionPorcentual, double rendimiento, double promedioPonderado, Proyecto proyecto)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Aportacion = aportacion;
            this.AportacionPorcentual = aportacionPorcentual;
            this.Rendimiento = rendimiento;
            this.PromedioPonderado = promedioPonderado;
            this.Proyecto = proyecto;
        }

        public Inversor() { }
    }
}
