﻿using Common.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class ActivoFijo
    {
        private int id;//4
        private string descripcion;//100 => 203
        private double costoActivo;//8
        private TipoActivoFijo tipoActivoFijo;//50 => 103
        private int vidaUtil;//4
        private double valorResidual;//8
        private Proyecto proyecto;//4
        //Total => 334

        public ActivoFijo(int id, string descripcion, double costoActivo, TipoActivoFijo tipoActivoFijo, int vidaUtil, double valorResidual, Proyecto proyecto)
        {
            this.id = id;
            this.descripcion = descripcion;
            this.costoActivo = costoActivo;
            this.tipoActivoFijo = tipoActivoFijo;
            this.vidaUtil = vidaUtil;
            this.valorResidual = valorResidual;
            this.proyecto = proyecto;
        }

        public ActivoFijo() { }

        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double CostoActivo { get => costoActivo; set => costoActivo = value; }
        public TipoActivoFijo TipoActivoFijo { get => tipoActivoFijo; set => tipoActivoFijo = value; }
        public int VidaUtil { get => vidaUtil; set => vidaUtil = value; }
        public double ValorResidual { get => valorResidual; set => valorResidual = value; }
        public Proyecto Proyecto { get => proyecto; set => proyecto = value; }
        public int Id { get => id; set => id = value; }
    }
}
