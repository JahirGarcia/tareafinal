﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public abstract class Repository
    {
        //header cliente
        protected BinaryReader brheader;
        protected BinaryWriter bwheader;
        //data cliente
        protected BinaryReader brdata;
        protected BinaryWriter bwdata;

        protected FileStream fsheader;
        protected FileStream fsdata;

        protected void Open(string FILENAME_HEADER, string FILENAME_DATA)
        {
            try
            {
                fsdata = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fsheader = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brheader = new BinaryReader(fsheader);
                    bwheader = new BinaryWriter(fsheader);

                    brdata = new BinaryReader(fsdata);
                    bwdata = new BinaryWriter(fsdata);

                    bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwheader.Write(0);//n
                    bwheader.Write(0);//k
                }
                else
                {
                    fsheader = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brheader = new BinaryReader(fsheader);
                    bwheader = new BinaryWriter(fsheader);
                    brdata = new BinaryReader(fsdata);
                    bwdata = new BinaryWriter(fsdata);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        protected void Close()
        {
            try
            {
                if (brdata != null)
                {
                    brdata.Close();
                }
                if (brheader != null)
                {
                    brheader.Close();
                }
                if (bwdata != null)
                {
                    bwdata.Close();
                }
                if (bwheader != null)
                {
                    bwheader.Close();
                }
                if (fsdata != null)
                {
                    fsdata.Close();
                }
                if (fsheader != null)
                {
                    fsheader.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
    }
}
