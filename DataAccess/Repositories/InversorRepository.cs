﻿using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class InversorRepository : Repository, IInversorRepository
    {
        private const string FILENAME_HEADER = "hinversor.dat";
        private const string FILENAME_DATA = "dinversor.dat";
        private const int SIZE = 107;

        public int Add(Inversor e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(e.Nombre);
            bwdata.Write(e.Aportacion);
            bwdata.Write(e.AportacionPorcentual);
            bwdata.Write(e.Rendimiento);
            bwdata.Write(e.PromedioPonderado);
            bwdata.Write(e.Proyecto.Id);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            Close();

            return k;
        }

        public int Delete(Inversor e)
        {
            throw new NotImplementedException();
        }

        public int Edit(Inversor e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == e.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(e.Id);
                    bwdata.Write(e.Nombre);
                    bwdata.Write(e.Aportacion);
                    bwdata.Write(e.AportacionPorcentual);
                    bwdata.Write(e.Rendimiento);
                    bwdata.Write(e.PromedioPonderado);
                    bwdata.Write(e.Proyecto.Id);
                    Close();

                    return e.Id;
                }
            }
            Close();

            return 0;
        }

        public IEnumerable<Inversor> GetAll()
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            List<Inversor> productos = new List<Inversor>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string nombre = brdata.ReadString();
                double aportacion = brdata.ReadDouble();
                double aportacionPorcentual = brdata.ReadDouble();
                double rendimiento = brdata.ReadDouble();
                double promedioPonderado = brdata.ReadDouble();
                int idProyecto = brdata.ReadInt32();
                Proyecto proyecto = new ProyectoRepository().GetById(idProyecto);
                Inversor inv = new Inversor(id, nombre, aportacion, aportacionPorcentual, rendimiento, promedioPonderado, proyecto);
                productos.Add(inv);
            }

            Close();
            return productos;
        }

        public IEnumerable<Inversor> GetByProyecto(int idProyecto)
        {
            throw new NotImplementedException();
        }
    }
}
