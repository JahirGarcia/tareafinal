﻿using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class IngresoRepository : Repository, IIngresoRepository
    {
        private const string FILENAME_HEADER = "hingreso.dat";
        private const string FILENAME_DATA = "dingreso.dat";
        private const int SIZE = 20;

        public int Add(Ingreso e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(e.Año);
            bwdata.Write(e.Monto);
            bwdata.Write(e.Proyecto.Id);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            Close();

            return k;
        }

        public int Delete(Ingreso e)
        {
            throw new NotImplementedException();
        }

        public int Edit(Ingreso e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == e.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(e.Id);
                    bwdata.Write(e.Año);
                    bwdata.Write(e.Monto);
                    bwdata.Write(e.Proyecto.Id);
                    Close();

                    return e.Id;
                }
            }
            Close();

            return 0;
        }

        public IEnumerable<Ingreso> GetAll()
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            List<Ingreso> ingresos = new List<Ingreso>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                int año = brdata.ReadInt32();
                double monto = brdata.ReadDouble();
                int idProyecto = brdata.ReadInt32();
                Proyecto proyecto = new ProyectoRepository().GetById(idProyecto);
                Ingreso ing = new Ingreso(id, año, monto, proyecto);
                ingresos.Add(ing);
            }

            Close();
            return ingresos;
        }

        public IEnumerable<Ingreso> GetByProyecto(int idProyecto)
        {
            throw new NotImplementedException();
        }
    }
}
