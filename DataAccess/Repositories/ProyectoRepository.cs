﻿using Common.ValueObjects;
using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ProyectoRepository : Repository, IProyectoRepository
    {
        private const string FILENAME_HEADER = "hproyecto.dat";
        private const string FILENAME_DATA = "dproyecto.dat";
        private const int SIZE = 361;

        public int Add(Proyecto e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(e.Nombre);
            bwdata.Write(e.Descripcion);
            bwdata.Write(e.InvercionInicial);
            bwdata.Write(e.MetodoDepreciacion.ToString());
            bwdata.Write(e.Tmar);
            bwdata.Write(e.Periodo);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            Close();

            return k;
        }

        public int Delete(Proyecto e)
        {
            throw new NotImplementedException();
        }

        public int Edit(Proyecto e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == e.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(e.Id);
                    bwdata.Write(e.Nombre);
                    bwdata.Write(e.Descripcion);
                    bwdata.Write(e.InvercionInicial);
                    bwdata.Write(e.MetodoDepreciacion.ToString());
                    bwdata.Write(e.Tmar);
                    bwdata.Write(e.Periodo);
                    Close();

                    return e.Id;
                }
            }
            Close();

            return 0;
        }

        public IEnumerable<Proyecto> GetAll()
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            List<Proyecto> productos = new List<Proyecto>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string nombre = brdata.ReadString();
                string descripcion = brdata.ReadString();
                double invercionInicial = brdata.ReadDouble();
                MetodoDepreciacion metodoDepreciacion = (MetodoDepreciacion)Enum.Parse(typeof(MetodoDepreciacion), brdata.ReadString());
                double tmar = brdata.ReadDouble();
                int periodo = brdata.ReadInt32();
                Proyecto p = new Proyecto(id, nombre,
                    descripcion, invercionInicial, metodoDepreciacion, tmar, periodo);
                productos.Add(p);
            }

            Close();
            return productos;
        }

        public Proyecto GetById(int id)
        {
            Proyecto p = new Proyecto();
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int ppos;
            for (ppos = 0; ppos < n; ppos++)
            {
                int pid = brheader.ReadInt32();
                if (pid == id)
                {
                    long dpos = ppos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string nombre = brdata.ReadString();
                    string descripcion = brdata.ReadString();
                    double invercionInicial = brdata.ReadDouble();
                    MetodoDepreciacion metodoDepreciacion = (MetodoDepreciacion)Enum.Parse(typeof(MetodoDepreciacion), brdata.ReadString());
                    double tmar = brdata.ReadDouble();
                    int periodo = brdata.ReadInt32();
                    p = new Proyecto(id, nombre,
                        descripcion, invercionInicial, metodoDepreciacion, tmar, periodo);

                    break;
                }
            }
            Close();

            return p;
        }
    }
}
