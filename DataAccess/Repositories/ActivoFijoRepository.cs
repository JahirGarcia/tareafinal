﻿using Common.ValueObjects;
using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ActivoFijoRepository : Repository, IActivoFijoRepository
    {
        private const string FILENAME_HEADER = "hactivifijo.dat";
        private const string FILENAME_DATA = "dactivofijo.dat";
        private const int SIZE = 334;

        public int Add(ActivoFijo e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(e.Descripcion);
            bwdata.Write(e.CostoActivo);
            bwdata.Write(e.TipoActivoFijo.ToString());
            bwdata.Write(e.VidaUtil.ToString());
            bwdata.Write(e.ValorResidual);
            bwdata.Write(e.Proyecto.Id);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            Close();

            return k;
        }

        public int Delete(ActivoFijo e)
        {
            throw new NotImplementedException();
        }

        public int Edit(ActivoFijo e)
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == e.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(e.Id);
                    bwdata.Write(e.Descripcion);
                    bwdata.Write(e.CostoActivo);
                    bwdata.Write(e.TipoActivoFijo.ToString());
                    bwdata.Write(e.VidaUtil.ToString());
                    bwdata.Write(e.ValorResidual);
                    bwdata.Write(e.Proyecto.Id);
                    Close();

                    return e.Id;
                }
            }
            Close();

            return 0;
        }

        public IEnumerable<ActivoFijo> GetAll()
        {
            Open(FILENAME_HEADER, FILENAME_DATA);
            List<ActivoFijo> productos = new List<ActivoFijo>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string descripcion = brdata.ReadString();
                double costoActivo = brdata.ReadInt32();
                TipoActivoFijo tipoActivoFijo = (TipoActivoFijo)Enum.Parse(typeof(TipoActivoFijo), brdata.ReadString());
                int vidaUtil = brdata.ReadInt32();
                double valorResidual = brdata.ReadDouble();
                int idProyecto = brdata.ReadInt32();
                Proyecto proyecto = new ProyectoRepository().GetById(id);

                ActivoFijo af = new ActivoFijo(id, descripcion, costoActivo, tipoActivoFijo, vidaUtil, valorResidual, proyecto);
                productos.Add(af);
            }

            Close();
            return productos;
        }

        public ActivoFijo GetById(int id)
        {
            ActivoFijo af = new ActivoFijo();
            Open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int ppos;
            for (ppos = 0; ppos < n; ppos++)
            {
                int pid = brheader.ReadInt32();
                if (pid == id)
                {
                    long dpos = ppos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string descripcion = brdata.ReadString();
                    double costoActivo = brdata.ReadInt32();
                    TipoActivoFijo tipoActivoFijo = (TipoActivoFijo)Enum.Parse(typeof(TipoActivoFijo), brdata.ReadString());
                    int vidaUtil = brdata.ReadInt32();
                    double valorResidual = brdata.ReadDouble();
                    int idProyecto = brdata.ReadInt32();
                    Proyecto proyecto = new ProyectoRepository().GetById(id);
                    af = new ActivoFijo(id, descripcion, costoActivo, tipoActivoFijo, vidaUtil, valorResidual, proyecto);

                    break;
                }
            }
            Close();

            return af;
        }

        public IEnumerable<ActivoFijo> GetByProyecto(int idProyecto)
        {
            throw new NotImplementedException();
        }
    }
}
