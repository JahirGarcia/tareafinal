﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Contracts
{
    public interface IActivoFijoRepository : IRepository<ActivoFijo>
    {
        ActivoFijo GetById(int id);
        IEnumerable<ActivoFijo> GetByProyecto(int idProyecto);
    }
}
