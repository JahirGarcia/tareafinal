﻿using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class InversorModel
    {
        private int id;
        private string nombre;
        private double aportacion;
        private double aportacionPorcentual;
        private double rendimiento;
        private double promedioPonderado;
        private ProyectoModel proyecto;

        private EntityState state;
        private IInversorRepository inversorRepository;

        public InversorModel()
        {
            this.inversorRepository = new InversorRepository();
        }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public double Aportacion { get => aportacion; set => aportacion = value; }
        public double AportacionPorcentual { get => aportacionPorcentual; set => aportacionPorcentual = value; }
        public double Rendimiento { get => rendimiento; set => rendimiento = value; }
        public double PromedioPonderado { get => promedioPonderado; set => promedioPonderado = value; }
        public ProyectoModel Proyecto { get => proyecto; set => proyecto = value; }
        public EntityState State { get => state; set => state = value; }

        public int SaveChanges()
        {
            Inversor p = new Inversor
            {
                Id = id,
                Nombre = nombre,
                Aportacion = aportacion,
                AportacionPorcentual = aportacionPorcentual,
                Rendimiento = rendimiento,
                PromedioPonderado = promedioPonderado,
                Proyecto = ProyectoModel.ToProyecto(proyecto)
            };

            int result = 0;
            switch (State)
            {
                case EntityState.Added:
                    {
                        result = inversorRepository.Add(p);
                        break;
                    }
                case EntityState.Modified:
                    {
                        result = inversorRepository.Edit(p);
                        break;
                    }
                case EntityState.Deleted:
                    {
                        result = inversorRepository.Delete(p);
                        break;
                    }
            }

            return result;
        }

        public List<InversorModel> GetAll()
        {
            List<InversorModel> listInversorModel = new List<InversorModel>();
            foreach (Inversor inv in inversorRepository.GetAll())
            {
                listInversorModel.Add(new InversorModel
                {
                    id = inv.Id,
                    nombre = inv.Nombre,
                    aportacion = inv.Aportacion,
                    aportacionPorcentual = inv.AportacionPorcentual,
                    rendimiento = inv.Rendimiento,
                    promedioPonderado = inv.PromedioPonderado,
                    proyecto = ProyectoModel.Maping(inv.Proyecto)
                });
            }

            return listInversorModel;
        }
    }
}
