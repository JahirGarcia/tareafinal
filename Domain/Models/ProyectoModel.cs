﻿using Common.ValueObjects;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ProyectoModel
    {
        private int id;
        private string nombre;
        private string descripcion;
        private double invercionInicial;
        private MetodoDepreciacion metodoDepreciacion;
        private double tmar;
        private int periodo;

        private EntityState state;
        private IProyectoRepository proyectoRepository;

        public ProyectoModel()
        {
            this.proyectoRepository = new ProyectoRepository();
        }

        public int Id { get => id; private set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double InvercionInicial { get => invercionInicial; set => invercionInicial = value; }
        public MetodoDepreciacion MetodoDepreciacion { get => metodoDepreciacion; set => metodoDepreciacion = value; }
        public double Tmar { get => tmar; set => tmar = value; }
        public int Periodo { get => periodo; set => periodo = value; }
        public EntityState State { private get => state; set => state = value; }

        public int SaveChanges()
        {
            Proyecto p = new Proyecto
            {
                Id = id,
                Nombre = nombre,
                Descripcion = descripcion,
                InvercionInicial = invercionInicial,
                MetodoDepreciacion = metodoDepreciacion,
                Tmar = tmar,
                Periodo = periodo
            };

            int result = 0;
            switch(State)
            {
                case EntityState.Added:
                {
                    result = proyectoRepository.Add(p);
                    break;
                }
                case EntityState.Modified:
                {
                    result = proyectoRepository.Edit(p);
                    break;
                }
                case EntityState.Deleted:
                {
                    result = proyectoRepository.Delete(p);
                    break;
                }
            }

            return result;
        }

        public List<ProyectoModel> GetAll()
        {
            List<ProyectoModel> listProyectoModel = new List<ProyectoModel>();
            foreach(Proyecto p in proyectoRepository.GetAll())
            {
                listProyectoModel.Add(new ProyectoModel
                {
                    id = p.Id,
                    nombre = p.Nombre,
                    descripcion = p.Descripcion,
                    invercionInicial = p.InvercionInicial,
                    metodoDepreciacion = p.MetodoDepreciacion,
                    tmar = p.Tmar,
                    periodo = p.Periodo
                });
            }

            return listProyectoModel;
        }

        public ProyectoModel GetById(int id)
        {
            Proyecto p = proyectoRepository.GetById(id);
            return new ProyectoModel
            {
                id = p.Id,
                nombre = p.Nombre,
                descripcion = p.Descripcion,
                invercionInicial = p.InvercionInicial,
                metodoDepreciacion = p.MetodoDepreciacion,
                tmar = p.Tmar,
                periodo = p.Periodo
            };
        }

        public static ProyectoModel Maping(Proyecto p)
        {
            return new ProyectoModel
            {
                id = p.Id,
                nombre = p.Nombre,
                descripcion = p.Descripcion,
                invercionInicial = p.InvercionInicial,
                metodoDepreciacion = p.MetodoDepreciacion,
                tmar = p.Tmar,
                periodo = p.Periodo
            };
        }

        public static Proyecto ToProyecto(ProyectoModel pm)
        {
            return new Proyecto
            {
                Id = pm.id,
                Nombre = pm.nombre,
                Descripcion = pm.descripcion,
                InvercionInicial = pm.invercionInicial,
                MetodoDepreciacion = pm.metodoDepreciacion,
                Tmar = pm.tmar,
                Periodo = pm.periodo
            };
        }
    }
}
