﻿using Common.ValueObjects;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ActivoFijoModel
    {
        private int id;
        private string descripcion;
        private double costoActivo;
        private TipoActivoFijo tipoActivoFijo;
        private int vidaUtil;
        private double valorResidual;
        private ProyectoModel proyecto;

        private EntityState state;
        private IActivoFijoRepository activoFijoRepository;

        public ActivoFijoModel()
        {
            this.activoFijoRepository = new ActivoFijoRepository();
        }

        public int Id { get => id; set => id = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double CostoActivo { get => costoActivo; set => costoActivo = value; }
        public TipoActivoFijo TipoActivoFijo { get => tipoActivoFijo; set => tipoActivoFijo = value; }
        public int VidaUtil { get => vidaUtil; set => vidaUtil = value; }
        public double ValorResidual { get => valorResidual; set => valorResidual = value; }
        public ProyectoModel Proyecto { get => proyecto; set => proyecto = value; }
        public EntityState State { get => state; set => state = value; }

        public int SaveChanges()
        {
            ActivoFijo af = new ActivoFijo
            {
                Id = id,
                Descripcion = descripcion,
                CostoActivo = costoActivo,
                TipoActivoFijo = tipoActivoFijo,
                VidaUtil = vidaUtil,
                ValorResidual = valorResidual,
                Proyecto = ProyectoModel.ToProyecto(proyecto)
            };

            int result = 0;
            switch (State)
            {
                case EntityState.Added:
                    {
                        result = activoFijoRepository.Add(af);
                        break;
                    }
                case EntityState.Modified:
                    {
                        result = activoFijoRepository.Edit(af);
                        break;
                    }
                case EntityState.Deleted:
                    {
                        result = activoFijoRepository.Delete(af);
                        break;
                    }
            }

            return result;
        }

        public List<ActivoFijoModel> GetAll()
        {
            List<ActivoFijoModel> listActivoFijoModel = new List<ActivoFijoModel>();
            foreach (ActivoFijo af in activoFijoRepository.GetAll())
            {
                listActivoFijoModel.Add(new ActivoFijoModel
                {
                    id = af.Id,
                    descripcion = af.Descripcion,
                    costoActivo = af.CostoActivo,
                    tipoActivoFijo = af.TipoActivoFijo,
                    vidaUtil = af.VidaUtil,
                    valorResidual = af.ValorResidual,
                    proyecto = ProyectoModel.Maping(af.Proyecto)
                });
            }

            return listActivoFijoModel;
        }
    }
}
