﻿using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class IngresoModel
    {
        private int id;
        private int año;
        private double monto;
        private ProyectoModel proyecto;

        private EntityState state;
        private IIngresoRepository ingresoRepository;

        public IngresoModel()
        {
            this.ingresoRepository = new IngresoRepository();
        }

        public int Id { get => id; set => id = value; }
        public int Año { get => año; set => año = value; }
        public double Monto { get => monto; set => monto = value; }
        public ProyectoModel Proyecto { get => proyecto; set => proyecto = value; }
        public EntityState State { get => state; set => state = value; }

        public int SaveChanges()
        {
            Ingreso p = new Ingreso
            {
                Id = id,
                Año = año,
                Monto = monto,
                Proyecto = ProyectoModel.ToProyecto(proyecto)
            };

            int result = 0;
            switch (State)
            {
                case EntityState.Added:
                    {
                        result = ingresoRepository.Add(p);
                        break;
                    }
                case EntityState.Modified:
                    {
                        result = ingresoRepository.Edit(p);
                        break;
                    }
                case EntityState.Deleted:
                    {
                        result = ingresoRepository.Delete(p);
                        break;
                    }
            }

            return result;
        }

        public List<IngresoModel> GetAll()
        {
            List<IngresoModel> listIngresoModel = new List<IngresoModel>();
            foreach (Ingreso ing in ingresoRepository.GetAll())
            {
                listIngresoModel.Add(new IngresoModel
                {
                    id = ing.Id,
                    año = ing.Año,
                    monto = ing.Monto,
                    proyecto = ProyectoModel.Maping(ing.Proyecto)
                });
            }

            return listIngresoModel;
        }
    }
}
