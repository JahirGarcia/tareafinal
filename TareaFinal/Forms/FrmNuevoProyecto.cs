﻿using Common.ValueObjects;
using Domain.Models;
using Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaFinal.Forms
{
    public partial class FrmNuevoProyecto : Form
    {
        private ProyectoModel proyectoModel = new ProyectoModel();
        private ActivoFijoModel activoFijoModel = new ActivoFijoModel();
        private InversorModel inversorModel = new InversorModel();
        private IngresoModel ingresoModel = new IngresoModel();
        public FrmNuevoProyecto()
        {
            InitializeComponent();
        }

        private void FrmNuevoProyecto_Load(object sender, EventArgs e)
        {
            cmbMetodoDepreciacion.DataSource = Enum.GetValues(typeof(MetodoDepreciacion));

            dgvActivosFijos.DataSource = dsGestionProyecto;
            dgvActivosFijos.DataMember = dsGestionProyecto.Tables["ActivoFijo"].TableName;

            dgvInversores.DataSource = dsGestionProyecto;
            dgvInversores.DataMember = dsGestionProyecto.Tables["Inversor"].TableName;

            dgvIngresos.DataSource = dsGestionProyecto;
            dgvIngresos.DataMember = dsGestionProyecto.Tables["Ingreso"].TableName;
        }

        private void BtnNuevoActivo_Click(object sender, EventArgs e)
        {
            FrmActivoFijo faf = new FrmActivoFijo();
            faf.DtActivoFijo = dsGestionProyecto.Tables["ActivoFijo"];
            faf.ShowDialog();
        }

        private void BtnEliminarActivo_Click(object sender, EventArgs e)
        {
            DeleteSelectedRow(dgvActivosFijos, dsGestionProyecto.Tables["ActivoFijo"], "Realmente desea eliminar este registro? (Los cambios efectuados en esta tabla puede ocacionar incongruencias en la informacion de la tabla de inversores)");
        }

        private void BtnEditarActivo_Click(object sender, EventArgs e)
        {
            DataRow drow = GetSelectedRow(dgvActivosFijos);

            if (drow != null)
            {
                DialogResult result = MessageBox.Show(this, "Realmente desea editar este registro? (Los cambios efectuados en esta tabla puede ocacionar incongruencias en la informacion de la tabla de inversores)", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    FrmActivoFijo faf = new FrmActivoFijo();
                    faf.DtActivoFijo = dsGestionProyecto.Tables["ActivoFijo"];
                    faf.DrActivoFijo = drow;
                    faf.ShowDialog();
                }
            }
        }

        private void BtnNuevoInversor_Click(object sender, EventArgs e)
        {
            FrmInversor fi = new FrmInversor();
            fi.InversionInicial = double.Parse(mskInversionInicial.Text);
            fi.DtInversor = dsGestionProyecto.Tables["Inversor"];
            fi.ShowDialog();
        }

        private void BtnEditarInversor_Click(object sender, EventArgs e)
        {
            DataRow drow = GetSelectedRow(dgvInversores);

            if (drow != null)
            {
                FrmInversor fi = new FrmInversor();
                fi.InversionInicial = double.Parse(mskInversionInicial.Text);
                fi.DtInversor = dsGestionProyecto.Tables["Inversor"];
                fi.DrInversor = drow;
                fi.ShowDialog();
            }
        }

        private void BtnEliminarInversor_Click(object sender, EventArgs e)
        {
            DeleteSelectedRow(dgvInversores, dsGestionProyecto.Tables["Inversor"], "Realmente desea eliminar este registro?");
        }

        private DataRow GetSelectedRow(DataGridView dataGridView)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            return drow;
        }

        private void DeleteSelectedRow(DataGridView dataGridView, DataTable dataTable, string warningMessage)
        {
            DataRow drow = GetSelectedRow(dataGridView);

            if (drow != null)
            {
                DialogResult result = MessageBox.Show(this, warningMessage, "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    dataTable.Rows.Remove(drow);
                    MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void DgvActivosFijos_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (dgvActivosFijos.Rows.Count > 0) {
                if(!gbInversores.Enabled)
                {
                    gbInversores.Enabled = true;
                }
            }
        }

        private void DgvActivosFijos_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgvActivosFijos.Rows.Count <= 0)
            {
                if(gbInversores.Enabled)
                {
                    gbInversores.Enabled = false;
                }
            }
        }

        private void ActializarInvercion()
        {
            double inversionInicial = 0;
            if (dgvActivosFijos.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvActivosFijos.Rows)
                {
                    DataRow drow = ((DataRowView)row.DataBoundItem).Row;
                    double costoActivo = double.Parse(drow["CostoActivo"].ToString());
                    inversionInicial += costoActivo;
                }
            }
            mskInversionInicial.Text = inversionInicial.ToString();
        }

        private void DgvActivosFijos_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            ActializarInvercion();
        }

        private void DgvInversores_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            ActualizarTMAR();
        }

        private void ActualizarTMAR()
        {
            double tmar = 0;
            if (dgvInversores.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvInversores.Rows)
                {
                    DataRow drow = ((DataRowView)row.DataBoundItem).Row;
                    double promedioPonderado = double.Parse(drow["PromedioPonderado"].ToString());
                    tmar += promedioPonderado;
                }
            }
            mskTMAR.Text = tmar.ToString();
        }

        private void NudPeriodo_ValueChanged(object sender, EventArgs e)
        {
            DataTable tablaIngresos = dsGestionProyecto.Tables["Ingreso"];
            int periodos = (int)nudPeriodo.Value;
            int totalFilas = tablaIngresos.Rows.Count;
            int diferencia = periodos - totalFilas;
            int filas = Math.Abs(diferencia);

            if (diferencia > 0)
            {
                for(int i=0; i<filas; i++)
                {
                    tablaIngresos.Rows.Add(tablaIngresos.Rows.Count, tablaIngresos.Rows.Count + 1, 0, 0);
                }
            } else
            {
                Console.WriteLine(tablaIngresos.Rows.Count);
                Console.WriteLine(filas);
                for (int i = 0; i < filas; i++)
                {
                    tablaIngresos.Rows.RemoveAt(tablaIngresos.Rows.Count - 1);
                }
            }
        }

        private void BtnGuardarProyecto_Click(object sender, EventArgs e)
        {
            string nombre = txtNombreProyecto.Text, descripcion = txtNombreProyecto.Text;
            double invercionInicial = double.Parse(mskInversionInicial.Text);
            double tmar = double.Parse(mskTMAR.Text);
            MetodoDepreciacion metodoDepreciacion = (MetodoDepreciacion)Enum.Parse(typeof(MetodoDepreciacion), cmbMetodoDepreciacion.Text);
            int periodo = (int)nudPeriodo.Value;
            int totalActivos = dsGestionProyecto.Tables["ActivoFijo"].Rows.Count;
            int totalInversores = dsGestionProyecto.Tables["Inversor"].Rows.Count;

            if(totalActivos > 0 && totalInversores > 0 && nombre != "" && periodo > 0)
            {
                proyectoModel.State = EntityState.Added;
                proyectoModel.Nombre = nombre;
                proyectoModel.Descripcion = descripcion;
                proyectoModel.InvercionInicial = invercionInicial;
                proyectoModel.MetodoDepreciacion = metodoDepreciacion;
                proyectoModel.Tmar = tmar;
                proyectoModel.Periodo = periodo;
                int idProyecto = proyectoModel.SaveChanges();

                foreach(DataRow row in dsGestionProyecto.Tables["ActivoFijo"].Rows)
                {
                    activoFijoModel.State = EntityState.Added;
                    activoFijoModel.Descripcion = descripcion;
                    activoFijoModel.CostoActivo = double.Parse(row["CostoActivo"].ToString());
                    activoFijoModel.TipoActivoFijo = (TipoActivoFijo)Enum.Parse(typeof(TipoActivoFijo), row["TipoActivoFijo"].ToString());
                    activoFijoModel.VidaUtil = int.Parse(row["VidaUtil"].ToString());
                    activoFijoModel.ValorResidual = double.Parse(row["ValorResidual"].ToString());
                    activoFijoModel.Proyecto = proyectoModel.GetById(idProyecto);
                    activoFijoModel.SaveChanges();
                }

                foreach (DataRow row in dsGestionProyecto.Tables["Inversor"].Rows)
                {
                    inversorModel.State = EntityState.Added;
                    inversorModel.Nombre = row["Nombre"].ToString();
                    inversorModel.Aportacion = double.Parse(row["Aportacion"].ToString());
                    inversorModel.AportacionPorcentual = double.Parse(row["AportacionPorcentual"].ToString());
                    inversorModel.Rendimiento = double.Parse(row["Rendimiento"].ToString());
                    inversorModel.PromedioPonderado = double.Parse(row["PromedioPonderado"].ToString());
                    inversorModel.Proyecto = proyectoModel.GetById(idProyecto);
                    inversorModel.SaveChanges();
                }

                foreach (DataRow row in dsGestionProyecto.Tables["Ingreso"].Rows)
                {
                    ingresoModel.State = EntityState.Added;
                    ingresoModel.Año = int.Parse(row["Año"].ToString());
                    ingresoModel.Monto = double.Parse(row["Ingreso"].ToString());
                    ingresoModel.Proyecto = proyectoModel.GetById(idProyecto);
                    ingresoModel.SaveChanges();
                }

                MessageBox.Show("El proyecto se ha guardado correctamente", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Asegurece de rellenar los campos correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
