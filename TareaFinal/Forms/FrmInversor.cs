﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaFinal.Forms
{
    public partial class FrmInversor : Form
    {
        private DataTable dtInversor;
        private DataRow drInversor;
        private double inversionInicial;

        public FrmInversor()
        {
            InitializeComponent();
        }

        public DataTable DtInversor { get => dtInversor; set => dtInversor = value; }
        public DataRow DrInversor {
            get => drInversor;
            set
            {
                drInversor = value;
            }
        }

        public double InversionInicial { get => inversionInicial; set => inversionInicial = value; }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text;
            double aportacion, aportacionPorcentual, rendimiento, promedioPonderado;

            if (double.TryParse(nudAportacion.Text, out aportacion) &&
                double.TryParse(mskRendimiento.Text, out rendimiento))
            {
                aportacionPorcentual = ((aportacion * 100) / inversionInicial) / 100;
                promedioPonderado = aportacionPorcentual * rendimiento;
                if (drInversor != null)
                {
                    DataRow drNew = dtInversor.NewRow();

                    int index = dtInversor.Rows.IndexOf(drInversor);
                    drNew["Id"] = drInversor["Id"];
                    drNew["Nombre"] = nombre;
                    drNew["Aportacion"] = aportacion;
                    drNew["AportacionPorcentual"] = aportacionPorcentual;
                    drNew["Rendimiento"] = rendimiento;
                    drNew["PromedioPonderado"] = promedioPonderado;
                    drNew["Proyecto"] = drInversor["Proyecto"];

                    dtInversor.Rows.RemoveAt(index);
                    dtInversor.Rows.InsertAt(drNew, index);
                    dtInversor.Rows[index].AcceptChanges();
                    dtInversor.Rows[index].SetModified();
                }
                else
                {
                    dtInversor.Rows.Add(dtInversor.Rows.Count + 1, nombre, aportacion, aportacionPorcentual, rendimiento, promedioPonderado, 0);
                }
                Dispose();
            }
            else
            {
                MessageBox.Show("Asegurece de rellenar los campos correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void FrmInversor_Load(object sender, EventArgs e)
        {
            nudAportacion.Maximum = (decimal)inversionInicial;
            if(drInversor != null) SetupFields();
        }

        private void SetupFields()
        {
            txtNombre.Text = drInversor["Nombre"].ToString();
            nudAportacion.Text = drInversor["Aportacion"].ToString();
            mskRendimiento.Text = drInversor["Rendimiento"].ToString();
        }
    }
}
