﻿using Common.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaFinal.Forms
{
    public partial class FrmActivoFijo : Form
    {
        private DataTable dtActivoFijo;
        private DataRow drActivoFijo;

        public DataTable DtActivoFijo { get => dtActivoFijo; set => dtActivoFijo = value; }
        public DataRow DrActivoFijo {
            get => drActivoFijo;
            set {
                drActivoFijo = value;
                txtDescripcion.Text = drActivoFijo["Descripcion"].ToString();
                mskCostoActivo.Text = drActivoFijo["CostoActivo"].ToString();
                cmbTipoActivoFijo.Text = drActivoFijo["TipoActivoFijo"].ToString();
                mskVidaUtil.Text = drActivoFijo["VidaUtil"].ToString();
                mskValorResidual.Text = drActivoFijo["ValorResidual"].ToString();
            }
        }

        public FrmActivoFijo()
        {
            InitializeComponent();
        }

        private void FrmActivoFijo_Load(object sender, EventArgs e)
        {
            cmbTipoActivoFijo.SelectedItem = cmbTipoActivoFijo.Items[13];
            TipoActivoFijo tipoActivo = (TipoActivoFijo)Enum.Parse(typeof(TipoActivoFijo), cmbTipoActivoFijo.Text);
            int vidaUtil = (int)tipoActivo;
            mskVidaUtil.Text = vidaUtil.ToString();
        }

        private void CmbTipoActivoFijo_SelectedIndexChanged(object sender, EventArgs e)
        {
            TipoActivoFijo tipoActivo = (TipoActivoFijo)Enum.Parse(typeof(TipoActivoFijo), cmbTipoActivoFijo.Text);
            int vidaUtil = (int)tipoActivo;
            mskVidaUtil.Text = vidaUtil.ToString();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            string descripcion = txtDescripcion.Text;
            int vidaUtil = int.Parse(mskVidaUtil.Text);
            double costoActivo, valorResidual;
            string tipoActivo = cmbTipoActivoFijo.Text;

            if (double.TryParse(mskCostoActivo.Text, out costoActivo) 
                && double.TryParse(mskValorResidual.Text, out valorResidual) 
                && descripcion.Length > 0)
            {
                if(DrActivoFijo != null)
                {
                    DataRow drNew = dtActivoFijo.NewRow();

                    int index = dtActivoFijo.Rows.IndexOf(drActivoFijo);
                    drNew["Id"] = drActivoFijo["Id"];
                    drNew["Descripcion"] = descripcion;
                    drNew["CostoActivo"] = costoActivo;
                    drNew["TipoActivoFijo"] = tipoActivo;
                    drNew["VidaUtil"] = vidaUtil;
                    drNew["ValorResidual"] = valorResidual;
                    drNew["Proyecto"] = drActivoFijo["Proyecto"];

                    dtActivoFijo.Rows.RemoveAt(index);
                    dtActivoFijo.Rows.InsertAt(drNew, index);
                    dtActivoFijo.Rows[index].AcceptChanges();
                    dtActivoFijo.Rows[index].SetModified();
                } else
                {
                    DtActivoFijo.Rows.Add(dtActivoFijo.Rows.Count + 1, descripcion, costoActivo, tipoActivo, vidaUtil, valorResidual, 0);
                }
                Dispose();
            } else
            {
                MessageBox.Show("Asegurece de rellenar los campos correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
