﻿namespace TareaFinal.Forms
{
    partial class FrmNuevoProyecto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnNuevoInversor = new System.Windows.Forms.Button();
            this.btnEditarInversor = new System.Windows.Forms.Button();
            this.btnEliminarInversor = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbInversores = new System.Windows.Forms.GroupBox();
            this.dgvInversores = new System.Windows.Forms.DataGridView();
            this.dsGestionProyecto = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnEliminarActivo = new System.Windows.Forms.Button();
            this.btnEditarActivo = new System.Windows.Forms.Button();
            this.btnNuevoActivo = new System.Windows.Forms.Button();
            this.dgvActivosFijos = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGuardarProyecto = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvIngresos = new System.Windows.Forms.DataGridView();
            this.nudPeriodo = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.mskTMAR = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbMetodoDepreciacion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mskInversionInicial = new System.Windows.Forms.MaskedTextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtNombreProyecto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.gbInversores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInversores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGestionProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActivosFijos)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIngresos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriodo)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNuevoInversor
            // 
            this.btnNuevoInversor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoInversor.Location = new System.Drawing.Point(175, 3);
            this.btnNuevoInversor.Name = "btnNuevoInversor";
            this.btnNuevoInversor.Size = new System.Drawing.Size(75, 23);
            this.btnNuevoInversor.TabIndex = 2;
            this.btnNuevoInversor.Text = "Nuevo";
            this.btnNuevoInversor.UseVisualStyleBackColor = true;
            this.btnNuevoInversor.Click += new System.EventHandler(this.BtnNuevoInversor_Click);
            // 
            // btnEditarInversor
            // 
            this.btnEditarInversor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarInversor.Location = new System.Drawing.Point(256, 3);
            this.btnEditarInversor.Name = "btnEditarInversor";
            this.btnEditarInversor.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInversor.TabIndex = 3;
            this.btnEditarInversor.Text = "Editar";
            this.btnEditarInversor.UseVisualStyleBackColor = true;
            this.btnEditarInversor.Click += new System.EventHandler(this.BtnEditarInversor_Click);
            // 
            // btnEliminarInversor
            // 
            this.btnEliminarInversor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarInversor.Location = new System.Drawing.Point(337, 3);
            this.btnEliminarInversor.Name = "btnEliminarInversor";
            this.btnEliminarInversor.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarInversor.TabIndex = 4;
            this.btnEliminarInversor.Text = "Eliminar";
            this.btnEliminarInversor.UseVisualStyleBackColor = true;
            this.btnEliminarInversor.Click += new System.EventHandler(this.BtnEliminarInversor_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnEliminarInversor);
            this.flowLayoutPanel1.Controls.Add(this.btnEditarInversor);
            this.flowLayoutPanel1.Controls.Add(this.btnNuevoInversor);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 193);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(415, 29);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // gbInversores
            // 
            this.gbInversores.Controls.Add(this.dgvInversores);
            this.gbInversores.Controls.Add(this.flowLayoutPanel1);
            this.gbInversores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbInversores.Enabled = false;
            this.gbInversores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbInversores.Location = new System.Drawing.Point(445, 249);
            this.gbInversores.Name = "gbInversores";
            this.gbInversores.Size = new System.Drawing.Size(427, 231);
            this.gbInversores.TabIndex = 6;
            this.gbInversores.TabStop = false;
            this.gbInversores.Text = "Inversores";
            // 
            // dgvInversores
            // 
            this.dgvInversores.AllowUserToAddRows = false;
            this.dgvInversores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInversores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInversores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInversores.Location = new System.Drawing.Point(6, 20);
            this.dgvInversores.MultiSelect = false;
            this.dgvInversores.Name = "dgvInversores";
            this.dgvInversores.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvInversores.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInversores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInversores.Size = new System.Drawing.Size(412, 167);
            this.dgvInversores.TabIndex = 6;
            this.dgvInversores.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.DgvInversores_RowStateChanged);
            // 
            // dsGestionProyecto
            // 
            this.dsGestionProyecto.DataSetName = "NewDataSet";
            this.dsGestionProyecto.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3,
            this.dataTable4});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7});
            this.dataTable1.TableName = "Proyecto";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn1.ColumnName = "Id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Nombre";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Descripcion";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Inversion Inicial";
            this.dataColumn4.ColumnName = "InversionInicial";
            this.dataColumn4.DataType = typeof(double);
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Metodo Depreciacion";
            this.dataColumn5.ColumnName = "MetodoDepreciacion";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "TMAR";
            this.dataColumn6.DataType = typeof(double);
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Periodo de Planeacion";
            this.dataColumn7.ColumnName = "PeriodoPlaneacion";
            this.dataColumn7.DataType = typeof(int);
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14});
            this.dataTable2.TableName = "ActivoFijo";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn8.ColumnName = "Id";
            this.dataColumn8.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Descripcion";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Costo del Activo";
            this.dataColumn10.ColumnName = "CostoActivo";
            this.dataColumn10.DataType = typeof(double);
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Tipo Activo Fijo";
            this.dataColumn11.ColumnName = "TipoActivoFijo";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Vida Util";
            this.dataColumn12.ColumnName = "VidaUtil";
            this.dataColumn12.DataType = typeof(int);
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Valor Residual";
            this.dataColumn13.ColumnName = "ValorResidual";
            this.dataColumn13.DataType = typeof(double);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn14.ColumnName = "Proyecto";
            this.dataColumn14.DataType = typeof(int);
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21});
            this.dataTable3.TableName = "Inversor";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn15.ColumnName = "Id";
            this.dataColumn15.DataType = typeof(int);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "Nombre";
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "Aportacion";
            this.dataColumn17.DataType = typeof(double);
            // 
            // dataColumn18
            // 
            this.dataColumn18.Caption = "Aportacion Porcentual";
            this.dataColumn18.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn18.ColumnName = "AportacionPorcentual";
            this.dataColumn18.DataType = typeof(double);
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "Rendimiento";
            this.dataColumn19.DataType = typeof(double);
            // 
            // dataColumn20
            // 
            this.dataColumn20.Caption = "Promedio Ponderado";
            this.dataColumn20.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn20.ColumnName = "PromedioPonderado";
            this.dataColumn20.DataType = typeof(double);
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn21.ColumnName = "Proyecto";
            this.dataColumn21.DataType = typeof(int);
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25});
            this.dataTable4.TableName = "Ingreso";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn22.ColumnName = "Id";
            this.dataColumn22.DataType = typeof(int);
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "Año";
            this.dataColumn23.DataType = typeof(int);
            this.dataColumn23.ReadOnly = true;
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "Ingreso";
            this.dataColumn24.DataType = typeof(double);
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn25.ColumnName = "Proyecto";
            this.dataColumn25.DataType = typeof(int);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel2);
            this.groupBox2.Controls.Add(this.dgvActivosFijos);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(445, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(427, 230);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Activos Fijos";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.btnEliminarActivo);
            this.flowLayoutPanel2.Controls.Add(this.btnEditarActivo);
            this.flowLayoutPanel2.Controls.Add(this.btnNuevoActivo);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 195);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(412, 29);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // btnEliminarActivo
            // 
            this.btnEliminarActivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarActivo.Location = new System.Drawing.Point(334, 3);
            this.btnEliminarActivo.Name = "btnEliminarActivo";
            this.btnEliminarActivo.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarActivo.TabIndex = 4;
            this.btnEliminarActivo.Text = "Eliminar";
            this.btnEliminarActivo.UseVisualStyleBackColor = true;
            this.btnEliminarActivo.Click += new System.EventHandler(this.BtnEliminarActivo_Click);
            // 
            // btnEditarActivo
            // 
            this.btnEditarActivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarActivo.Location = new System.Drawing.Point(253, 3);
            this.btnEditarActivo.Name = "btnEditarActivo";
            this.btnEditarActivo.Size = new System.Drawing.Size(75, 23);
            this.btnEditarActivo.TabIndex = 3;
            this.btnEditarActivo.Text = "Editar";
            this.btnEditarActivo.UseVisualStyleBackColor = true;
            this.btnEditarActivo.Click += new System.EventHandler(this.BtnEditarActivo_Click);
            // 
            // btnNuevoActivo
            // 
            this.btnNuevoActivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoActivo.Location = new System.Drawing.Point(172, 3);
            this.btnNuevoActivo.Name = "btnNuevoActivo";
            this.btnNuevoActivo.Size = new System.Drawing.Size(75, 23);
            this.btnNuevoActivo.TabIndex = 2;
            this.btnNuevoActivo.Text = "Nuevo";
            this.btnNuevoActivo.UseVisualStyleBackColor = true;
            this.btnNuevoActivo.Click += new System.EventHandler(this.BtnNuevoActivo_Click);
            // 
            // dgvActivosFijos
            // 
            this.dgvActivosFijos.AllowUserToAddRows = false;
            this.dgvActivosFijos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvActivosFijos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActivosFijos.Location = new System.Drawing.Point(6, 20);
            this.dgvActivosFijos.MultiSelect = false;
            this.dgvActivosFijos.Name = "dgvActivosFijos";
            this.dgvActivosFijos.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvActivosFijos.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvActivosFijos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvActivosFijos.Size = new System.Drawing.Size(412, 169);
            this.dgvActivosFijos.TabIndex = 0;
            this.dgvActivosFijos.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DgvActivosFijos_RowsAdded);
            this.dgvActivosFijos.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DgvActivosFijos_RowsRemoved);
            this.dgvActivosFijos.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.DgvActivosFijos_RowStateChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.flowLayoutPanel3);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.nudPeriodo);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.mskTMAR);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cmbMetodoDepreciacion);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.mskInversionInicial);
            this.groupBox3.Controls.Add(this.txtDescripcion);
            this.groupBox3.Controls.Add(this.txtNombreProyecto);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(13, 13);
            this.groupBox3.Name = "groupBox3";
            this.tableLayoutPanel1.SetRowSpan(this.groupBox3, 2);
            this.groupBox3.Size = new System.Drawing.Size(426, 467);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos del Proyecto";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.Controls.Add(this.btnGuardarProyecto);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(9, 429);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(411, 29);
            this.flowLayoutPanel3.TabIndex = 15;
            // 
            // btnGuardarProyecto
            // 
            this.btnGuardarProyecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarProyecto.Location = new System.Drawing.Point(258, 3);
            this.btnGuardarProyecto.Name = "btnGuardarProyecto";
            this.btnGuardarProyecto.Size = new System.Drawing.Size(150, 23);
            this.btnGuardarProyecto.TabIndex = 0;
            this.btnGuardarProyecto.Text = "Guardar Proyecto";
            this.btnGuardarProyecto.UseVisualStyleBackColor = true;
            this.btnGuardarProyecto.Click += new System.EventHandler(this.BtnGuardarProyecto_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.dgvIngresos);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(9, 285);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox4.Size = new System.Drawing.Size(411, 138);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Proyeccion de Ingresos";
            // 
            // dgvIngresos
            // 
            this.dgvIngresos.AllowUserToAddRows = false;
            this.dgvIngresos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvIngresos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIngresos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvIngresos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIngresos.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvIngresos.Location = new System.Drawing.Point(11, 18);
            this.dgvIngresos.MultiSelect = false;
            this.dgvIngresos.Name = "dgvIngresos";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIngresos.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvIngresos.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvIngresos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIngresos.Size = new System.Drawing.Size(389, 109);
            this.dgvIngresos.TabIndex = 1;
            // 
            // nudPeriodo
            // 
            this.nudPeriodo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudPeriodo.Location = new System.Drawing.Point(121, 244);
            this.nudPeriodo.Name = "nudPeriodo";
            this.nudPeriodo.Size = new System.Drawing.Size(299, 21);
            this.nudPeriodo.TabIndex = 13;
            this.nudPeriodo.ValueChanged += new System.EventHandler(this.NudPeriodo_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Prd. de Planeacion:";
            // 
            // mskTMAR
            // 
            this.mskTMAR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mskTMAR.BackColor = System.Drawing.SystemColors.Info;
            this.mskTMAR.Enabled = false;
            this.mskTMAR.Location = new System.Drawing.Point(121, 217);
            this.mskTMAR.Name = "mskTMAR";
            this.mskTMAR.Size = new System.Drawing.Size(299, 21);
            this.mskTMAR.TabIndex = 11;
            this.mskTMAR.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "TMAR:";
            // 
            // cmbMetodoDepreciacion
            // 
            this.cmbMetodoDepreciacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMetodoDepreciacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMetodoDepreciacion.FormattingEnabled = true;
            this.cmbMetodoDepreciacion.Location = new System.Drawing.Point(121, 185);
            this.cmbMetodoDepreciacion.Name = "cmbMetodoDepreciacion";
            this.cmbMetodoDepreciacion.Size = new System.Drawing.Size(299, 23);
            this.cmbMetodoDepreciacion.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Mtd. de Depreciacion:";
            // 
            // mskInversionInicial
            // 
            this.mskInversionInicial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mskInversionInicial.BackColor = System.Drawing.SystemColors.Info;
            this.mskInversionInicial.Enabled = false;
            this.mskInversionInicial.Location = new System.Drawing.Point(121, 158);
            this.mskInversionInicial.Name = "mskInversionInicial";
            this.mskInversionInicial.Size = new System.Drawing.Size(299, 21);
            this.mskInversionInicial.TabIndex = 7;
            this.mskInversionInicial.Text = "0";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescripcion.Location = new System.Drawing.Point(121, 52);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(299, 100);
            this.txtDescripcion.TabIndex = 6;
            // 
            // txtNombreProyecto
            // 
            this.txtNombreProyecto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNombreProyecto.Location = new System.Drawing.Point(121, 25);
            this.txtNombreProyecto.Name = "txtNombreProyecto";
            this.txtNombreProyecto.Size = new System.Drawing.Size(299, 21);
            this.txtNombreProyecto.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Inversion Inicial:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Descripcion:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre del Proyecto:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gbInversores, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(885, 493);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // FrmNuevoProyecto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 493);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(901, 532);
            this.Name = "FrmNuevoProyecto";
            this.Text = "Nuevo Proyecto";
            this.Load += new System.EventHandler(this.FrmNuevoProyecto_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.gbInversores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInversores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGestionProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvActivosFijos)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIngresos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriodo)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNuevoInversor;
        private System.Windows.Forms.Button btnEditarInversor;
        private System.Windows.Forms.Button btnEliminarInversor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbInversores;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnEliminarActivo;
        private System.Windows.Forms.Button btnEditarActivo;
        private System.Windows.Forms.Button btnNuevoActivo;
        private System.Windows.Forms.DataGridView dgvActivosFijos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox mskInversionInicial;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtNombreProyecto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox cmbMetodoDepreciacion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudPeriodo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox mskTMAR;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button btnGuardarProyecto;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Data.DataSet dsGestionProyecto;
        private System.Data.DataTable dataTable1;
        private System.Data.DataTable dataTable2;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Windows.Forms.DataGridView dgvInversores;
        private System.Windows.Forms.DataGridView dgvIngresos;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
    }
}