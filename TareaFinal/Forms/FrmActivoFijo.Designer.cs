﻿namespace TareaFinal.Forms
{
    partial class FrmActivoFijo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.mskCostoActivo = new System.Windows.Forms.MaskedTextBox();
            this.cmbTipoActivoFijo = new System.Windows.Forms.ComboBox();
            this.mskValorResidual = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.mskVidaUtil = new System.Windows.Forms.MaskedTextBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descripcion:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tipo de Activo Fijo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Costo del Activo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Valor de Salvamento/Residual:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescripcion.Location = new System.Drawing.Point(172, 12);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(420, 20);
            this.txtDescripcion.TabIndex = 4;
            // 
            // mskCostoActivo
            // 
            this.mskCostoActivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mskCostoActivo.Location = new System.Drawing.Point(172, 38);
            this.mskCostoActivo.Name = "mskCostoActivo";
            this.mskCostoActivo.Size = new System.Drawing.Size(420, 20);
            this.mskCostoActivo.TabIndex = 5;
            // 
            // cmbTipoActivoFijo
            // 
            this.cmbTipoActivoFijo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipoActivoFijo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoActivoFijo.FormattingEnabled = true;
            this.cmbTipoActivoFijo.Items.AddRange(new object[] {
            "ASCENSOR_ELEVADOR_Y_UNIDAD_DE_AIRE_ACONDICIONADO",
            "EDIFICIO_COMERCIAL",
            "OTRA_MAQUINARIA_Y_EQUIPO",
            "BIEN_INMOVIL",
            "EDIFICIO_DE_ALQUILER",
            "EDIFICIO_INDUSTRIAL",
            "EQUIPO_DE_EMPRESA_AGROINDUSTRIAL",
            "EQUIPO_DE_COMPUTO",
            "EQUIPO_DE_COMUNICACION",
            "EQUIPO_PARA_MEDIOS_DE_COMUNICACION",
            "INSTALACION_EN_EXPLOTACIONES_AGROPECUARIAS",
            "MAQUINARIA_AGRICOLA",
            "MAQUINARIA_Y_EQUIPO_NO_ADHERIDO_PERMANENTEMENTE",
            "MOBILIARIO_Y_EQUIPO_DE_OFICINA",
            "OTRO_EQUIPO_DE_TRANSPORTE",
            "RESIDENCIA_EN_EXPLOTACIONES_AGROPECUARIAS",
            "TRANSPORTE_COLECTIVO_O_DE_CARGA",
            "VEHICULO_DE_EMPRESA_DE_ALQUILER",
            "VEHICULO_DE_USO_PARTICULAR"});
            this.cmbTipoActivoFijo.Location = new System.Drawing.Point(172, 64);
            this.cmbTipoActivoFijo.Name = "cmbTipoActivoFijo";
            this.cmbTipoActivoFijo.Size = new System.Drawing.Size(420, 21);
            this.cmbTipoActivoFijo.TabIndex = 6;
            this.cmbTipoActivoFijo.SelectedIndexChanged += new System.EventHandler(this.CmbTipoActivoFijo_SelectedIndexChanged);
            // 
            // mskValorResidual
            // 
            this.mskValorResidual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mskValorResidual.Location = new System.Drawing.Point(172, 117);
            this.mskValorResidual.Name = "mskValorResidual";
            this.mskValorResidual.Size = new System.Drawing.Size(420, 20);
            this.mskValorResidual.TabIndex = 7;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnCancelar);
            this.flowLayoutPanel1.Controls.Add(this.btnAceptar);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 149);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(580, 29);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(502, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 10;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(421, 3);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 9;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Vida Util:";
            // 
            // mskVidaUtil
            // 
            this.mskVidaUtil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mskVidaUtil.BackColor = System.Drawing.SystemColors.Info;
            this.mskVidaUtil.Enabled = false;
            this.mskVidaUtil.Location = new System.Drawing.Point(172, 91);
            this.mskVidaUtil.Name = "mskVidaUtil";
            this.mskVidaUtil.Size = new System.Drawing.Size(420, 20);
            this.mskVidaUtil.TabIndex = 10;
            // 
            // FrmActivoFijo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 190);
            this.Controls.Add(this.mskVidaUtil);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.mskValorResidual);
            this.Controls.Add(this.cmbTipoActivoFijo);
            this.Controls.Add(this.mskCostoActivo);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(620, 229);
            this.Name = "FrmActivoFijo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Activo Fijo";
            this.Load += new System.EventHandler(this.FrmActivoFijo_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.MaskedTextBox mskCostoActivo;
        private System.Windows.Forms.ComboBox cmbTipoActivoFijo;
        private System.Windows.Forms.MaskedTextBox mskValorResidual;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox mskVidaUtil;
    }
}