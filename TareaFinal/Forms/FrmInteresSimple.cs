﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TareaFinal.Utilities;

namespace TareaFinal
{
    public partial class FrmInteresSimple : Form
    {
        public FrmInteresSimple()
        {
            InitializeComponent();
        }

        private void BtnCalcular_Click(object sender, EventArgs e)
        {
            double tasa, periodo, presente, futuro;
            string opcion = cmbOpciones.SelectedItem.ToString();

            if(opcion == "PRESENTE")
            {
                if(double.TryParse(mskTasa.Text, out tasa) && double.TryParse(mskPeriodo.Text, out periodo) &&
                    double.TryParse(mskFuturo.Text, out futuro))
                {
                    presente = InteresSimple.CalcularPresente(tasa, futuro, periodo);
                    mskPresente.Text = presente.ToString();
                    return;
                }
            } else if(opcion == "FUTURO")
            {
                if (double.TryParse(mskTasa.Text, out tasa) && double.TryParse(mskPeriodo.Text, out periodo) &&
                    double.TryParse(mskPresente.Text, out presente))
                {
                    futuro = InteresSimple.CalcularFuturo(presente, tasa, periodo);
                    mskFuturo.Text = futuro.ToString();
                    return;
                }
            } else if(opcion == "TASA")
            {
                if (double.TryParse(mskPresente.Text, out presente) && double.TryParse(mskPeriodo.Text, out periodo) &&
                    double.TryParse(mskFuturo.Text, out futuro))
                {
                    tasa = InteresSimple.CalcularTasa(presente, futuro, periodo);
                    mskTasa.Text = tasa.ToString();
                    return;
                }
            } else if(opcion == "PERIODO")
            {
                if (double.TryParse(mskTasa.Text, out tasa) && double.TryParse(mskPresente.Text, out presente) &&
                    double.TryParse(mskFuturo.Text, out futuro))
                {
                    periodo = InteresSimple.CalcularPeriodo(presente, futuro, tasa);
                    mskPeriodo.Text = periodo.ToString();
                    return;
                }
            }

            MessageBox.Show("Ingrese los valores correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void FrmInteresSimple_Load(object sender, EventArgs e)
        {
            cmbOpciones.SelectedItem = cmbOpciones.Items[0];
        }
    }
}
