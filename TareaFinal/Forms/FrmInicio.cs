﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TareaFinal.Forms;
using TareaFinal.Views;

namespace TareaFinal
{
    public partial class FrmInicio : Form
    {
        public FrmInicio()
        {
            InitializeComponent();
        }

        private void InteresSimpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmInteresSimple fis = new FrmInteresSimple();
            fis.MdiParent = this;
            fis.Show();
        }

        private void InteresCompuestoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmInteresCompuesto fic = new FrmInteresCompuesto();
            fic.MdiParent = this;
            fic.Show();
        }

        private void AnualidadSimpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAnualidadSimple fas = new FrmAnualidadSimple();
            fas.MdiParent = this;
            fas.Show();
        }

        private void AnualidadConGradienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAnualidadGradiente fag = new FrmAnualidadGradiente();
            fag.MdiParent = this;
            fag.Show();
        }

        private void ConversorDeTasasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConversorTasas fcs = new FrmConversorTasas();
            fcs.MdiParent = this;
            fcs.Show();
        }

        private void NuevoProyectoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNuevoProyecto fnp = new FrmNuevoProyecto();
            fnp.MdiParent = this;
            fnp.Show();
        }
    }
}
