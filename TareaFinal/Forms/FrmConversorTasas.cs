﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TareaFinal.Utilities;

namespace TareaFinal.Views
{
    public partial class FrmConversorTasas : Form
    {
        public FrmConversorTasas()
        {
            InitializeComponent();
        }

        private void FrmConversorTasas_Load(object sender, EventArgs e)
        {
            cmbTipoTasa1.DataSource = Enum.GetValues(typeof(TipoTasa));
            cmbTipoTasa2.DataSource = Enum.GetValues(typeof(TipoTasa));
            cmbTipoCap1.DataSource = Enum.GetValues(typeof(TipoCapitalizacion));
            cmbTipoCap2.DataSource = Enum.GetValues(typeof(TipoCapitalizacion));
        }

        private void BtnCalcular_Click(object sender, EventArgs e)
        {
            double tasa1, tasa2 = 0;
            TipoTasa tipoTasa1 = (TipoTasa)cmbTipoTasa1.SelectedItem;
            TipoTasa tipoTasa2 = (TipoTasa)cmbTipoTasa2.SelectedItem;
            TipoCapitalizacion tipoCap1 = (TipoCapitalizacion)cmbTipoCap1.SelectedItem;
            TipoCapitalizacion tipoCap2 = (TipoCapitalizacion)cmbTipoCap2.SelectedItem;

            if (double.TryParse(mskTasa1.Text, out tasa1))
            {
                if(tipoTasa1 == TipoTasa.NOMINAL && tipoTasa2 == TipoTasa.NOMINAL)
                {
                    tasa2 = ConversorTasas.Convertir(tasa1, ((double)tipoCap1 / (double)tipoCap2), 1);
                } else if(tipoTasa1 == TipoTasa.EFECTIVA && tipoTasa2 == TipoTasa.EFECTIVA)
                {
                    tasa2 = ConversorTasas.Convertir(tasa1, 1, ((double)tipoCap1 / (double)tipoCap2));
                } else if(tipoTasa1 == TipoTasa.NOMINAL && tipoTasa2 == TipoTasa.EFECTIVA)
                {
                    tasa2 = ConversorTasas.Convertir(tasa1, ((double)tipoCap1 / (double)tipoCap2), 1);
                } else if(tipoTasa1 == TipoTasa.EFECTIVA && tipoTasa2 == TipoTasa.NOMINAL)
                {
                    tasa2 = ConversorTasas.Convertir(tasa1, ((double)tipoCap1 / (double)tipoCap2), 1);
                }
                mskTasa2.Text = tasa2.ToString();
            } else
            {
                MessageBox.Show("Ingrese los valores correctamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
