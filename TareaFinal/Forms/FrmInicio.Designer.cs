﻿namespace TareaFinal
{
    partial class FrmInicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.calculadoraFinancieraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interesSimpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interesCompuestoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anualidadSimpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anualidadConGradienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conversorDeTasasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flujoNetoDeEfectivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoProyectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proyectosGuardadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculadoraFinancieraToolStripMenuItem,
            this.flujoNetoDeEfectivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // calculadoraFinancieraToolStripMenuItem
            // 
            this.calculadoraFinancieraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.interesSimpleToolStripMenuItem,
            this.interesCompuestoToolStripMenuItem,
            this.anualidadSimpleToolStripMenuItem,
            this.anualidadConGradienteToolStripMenuItem,
            this.conversorDeTasasToolStripMenuItem});
            this.calculadoraFinancieraToolStripMenuItem.Name = "calculadoraFinancieraToolStripMenuItem";
            this.calculadoraFinancieraToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.calculadoraFinancieraToolStripMenuItem.Text = "Calculadora Financiera";
            // 
            // interesSimpleToolStripMenuItem
            // 
            this.interesSimpleToolStripMenuItem.Name = "interesSimpleToolStripMenuItem";
            this.interesSimpleToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.interesSimpleToolStripMenuItem.Text = "Interes Simple";
            this.interesSimpleToolStripMenuItem.Click += new System.EventHandler(this.InteresSimpleToolStripMenuItem_Click);
            // 
            // interesCompuestoToolStripMenuItem
            // 
            this.interesCompuestoToolStripMenuItem.Name = "interesCompuestoToolStripMenuItem";
            this.interesCompuestoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.interesCompuestoToolStripMenuItem.Text = "Interes Compuesto";
            this.interesCompuestoToolStripMenuItem.Click += new System.EventHandler(this.InteresCompuestoToolStripMenuItem_Click);
            // 
            // anualidadSimpleToolStripMenuItem
            // 
            this.anualidadSimpleToolStripMenuItem.Name = "anualidadSimpleToolStripMenuItem";
            this.anualidadSimpleToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.anualidadSimpleToolStripMenuItem.Text = "Anualidad Simple";
            this.anualidadSimpleToolStripMenuItem.Click += new System.EventHandler(this.AnualidadSimpleToolStripMenuItem_Click);
            // 
            // anualidadConGradienteToolStripMenuItem
            // 
            this.anualidadConGradienteToolStripMenuItem.Name = "anualidadConGradienteToolStripMenuItem";
            this.anualidadConGradienteToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.anualidadConGradienteToolStripMenuItem.Text = "Anualidad con Gradiente";
            this.anualidadConGradienteToolStripMenuItem.Click += new System.EventHandler(this.AnualidadConGradienteToolStripMenuItem_Click);
            // 
            // conversorDeTasasToolStripMenuItem
            // 
            this.conversorDeTasasToolStripMenuItem.Name = "conversorDeTasasToolStripMenuItem";
            this.conversorDeTasasToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.conversorDeTasasToolStripMenuItem.Text = "Conversor de Tasas";
            this.conversorDeTasasToolStripMenuItem.Click += new System.EventHandler(this.ConversorDeTasasToolStripMenuItem_Click);
            // 
            // flujoNetoDeEfectivoToolStripMenuItem
            // 
            this.flujoNetoDeEfectivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoProyectoToolStripMenuItem,
            this.proyectosGuardadosToolStripMenuItem});
            this.flujoNetoDeEfectivoToolStripMenuItem.Name = "flujoNetoDeEfectivoToolStripMenuItem";
            this.flujoNetoDeEfectivoToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.flujoNetoDeEfectivoToolStripMenuItem.Text = "Gestion de Proyectos";
            // 
            // nuevoProyectoToolStripMenuItem
            // 
            this.nuevoProyectoToolStripMenuItem.Name = "nuevoProyectoToolStripMenuItem";
            this.nuevoProyectoToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.nuevoProyectoToolStripMenuItem.Text = "Nuevo Proyecto";
            this.nuevoProyectoToolStripMenuItem.Click += new System.EventHandler(this.NuevoProyectoToolStripMenuItem_Click);
            // 
            // proyectosGuardadosToolStripMenuItem
            // 
            this.proyectosGuardadosToolStripMenuItem.Name = "proyectosGuardadosToolStripMenuItem";
            this.proyectosGuardadosToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.proyectosGuardadosToolStripMenuItem.Text = "Proyectos Guardados";
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmInicio";
            this.Text = "Inicio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem calculadoraFinancieraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interesSimpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interesCompuestoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anualidadSimpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anualidadConGradienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conversorDeTasasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flujoNetoDeEfectivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoProyectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proyectosGuardadosToolStripMenuItem;
    }
}

