﻿using System;

namespace TareaFinal.Utilities
{
    class ConversorTasas
    {
        public static double Convertir(double tasa, double capNom, double capEfec)
        {
            return Math.Pow((1 + tasa / capNom), capEfec) - 1;
        }
    }
}