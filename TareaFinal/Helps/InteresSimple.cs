﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareaFinal.Utilities
{
    class InteresSimple
    {
        public static double CalcularTasa(double presente, double futuro, double periodo)
        {
            return (futuro - presente) / (presente * periodo);
        }

        public static double CalcularPeriodo(double presente, double futuro, double tasa)
        {
            return (futuro - presente) / (presente * tasa);
        }

        public static double CalcularPresente(double tasa, double futuro, double periodo)
        {
            return futuro / (1 + tasa * periodo);
        }

        public static double CalcularFuturo(double presente, double tasa, double periodo)
        {
            return presente * (1 + tasa * periodo);
        }
    }
}
