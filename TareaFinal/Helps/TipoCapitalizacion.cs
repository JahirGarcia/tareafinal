﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareaFinal.Utilities
{
    enum TipoCapitalizacion
    {
        MENSUAL = 30,
        BIMENSUAL = 60,
        TRIMESTRAL = 90,
        CUATRIMESTRAL = 120,
        SEMESTRAL = 180,
        ANUAL = 360
    }
}
