﻿using System;

namespace TareaFinal.Utilities
{
    class InteresCompuesto
    {
        public static double CalcularTasa(double presente, double futuro, double periodo)
        {
            return Math.Pow((futuro / presente), (1 / periodo)) - 1;
        }

        public static double CalcularPeriodo(double presente, double futuro, double tasa)
        {
            return Math.Log((futuro / presente)) / Math.Log((1 + tasa));
        }

        public static double CalcularPresente(double tasa, double futuro, double periodo)
        {
            return futuro / Math.Pow((1 + tasa), periodo);
        }

        public static double CalcularFuturo(double presente, double tasa, double periodo)
        {
            return presente * Math.Pow((1 + tasa), periodo);
        }
    }
}